package problems;

/*
    PROBLEM
        Given an array of ints in which each element should be 1 greater than
        the previous element, return the int that is missing in the sequence.
        Elements of the array may be duplicated, but that is not considered a
        break in the sequence. If the sequence does not have any missing elements,
        return 0;

    EXAMPLE
        missingNumberInRange([1,2,3,4,5]) => 0
        missingNumberInRange([1,2,4,5]) => 3
        missingNumberInRange([1,2,3,3,5]) => 4

*/

public class MissingNumberInRange {
    public static int missingNumberInRange(int[] nums){
        return 1337;
    }
}
