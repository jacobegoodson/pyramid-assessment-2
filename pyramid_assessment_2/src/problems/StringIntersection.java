package problems;

/*
    PROBLEM
        Find the intersection of two strings in an array. An intersection is where
        there is a value in one string that matches the value in another string.
        Remember that intersections do not return duplicates. The strings represent a list
        of numbers, you must return a string that represents a list of sorted numbers in
        ascending order.

    EXAMPLE
        stringIntersection(["1, 2, 3" "4, 5, 6"]) => ""
        stringIntersection(["" "4, 5, 6"]) => ""
        stringIntersection(["1" "1"]) => "1"
        stringIntersection(["1, 1, 1" "1, 1, 1, 1"]) => "1"
        stringIntersection(["1, 2, 3" "4, 5, 3"]) => "3"
        stringIntersection(["1, 2, 3" "2, 5, 3"]) => "2, 3"
        stringIntersection(["1, 2, 3, 8" "2, 5, 3, 8"]) => "2, 3, 8"

    IMPORTANT
        The output must return formatted just as it is above, notice that is must be sorted, you may
        use any technique to solve this problem.
*/

public class StringIntersection {
    public static String stringIntersection(String[] arr){
        return "";
    }
}
