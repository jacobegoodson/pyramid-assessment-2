package problems;

/*
    PROBLEM
        Check whether or not every character in a string is a number.

    EXAMPLE
        onlyDigitsInString("") => false
        onlyDigitsInString("1a") => false
        onlyDigitsInString("123") => true
        onlyDigitsInString("1") => true
        onlyDigitsInString("1432546547a4324") => false

*/

public class OnlyDigitsInString {
    public static boolean onlyDigitsInString(String s){
        return false;
    }
}
