package problems;

/*
    PROBLEM
        Complete the method below that takes an array containing two strings where
        each string represents keypresses separated by commas. For this problem,
        a keypress can be either a printable character or a backspace
        (represented by -B). Your function should determine if the two strings of
        keypresses are equivalent.

    EXAMPLE
        checkEquivalentKeypresses(["a,b,c,d", "a,b,c,c,-B,d"]) => true
        checkEquivalentKeypresses(["-B,-B,-B,c,c", "c,c"]) => true
        checkEquivalentKeypresses(["", "a,-B,-B,a,-B,a,b,c,c,c,d"]) => false

*/

public class CheckEquivalentKeypresses {
    public static boolean checkEquivalentKeypresses(String[] keysPressed){
        return false;
    }
}
