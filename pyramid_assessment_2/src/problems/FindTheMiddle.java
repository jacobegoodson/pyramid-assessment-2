package problems;

/*
    PROBLEM
        Given an array of natural numbers, find the middle number in the array.
        If the array contains an even number of integers, there is no middle, return 0.
        If the array is empty, return 0.

    EXAMPLE
        findTheMiddle([1 2 3]) => 2
        findTheMiddle([1 0 3]) => 0
        findTheMiddle([1 2]) => 0
        findTheMiddle([]) => 0

*/

public class FindTheMiddle {
    public static int findTheMiddle(int[] nums){
        return 0;
    }
}
