package problems;

/*
    PROBLEM
        The method below must return Hello concatenated with the argument that is
        given to the method.

    EXAMPLE
        helloWhatever("hi") => "Hello, hi"
        helloWhatever("there") => "Hello, there"
        helloWhatever("") => "Hello, "
        helloWhatever("SPAM") => "Hello, SPAM"

*/

public class HelloWhatever {
    public static String helloWhatever(String s){
        return "you better get this one!";
    }
}
