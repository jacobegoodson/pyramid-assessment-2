package problems;

/*
    PROBLEM
        Method fizzBuzzBazzFizzle(int n) takes an int and returns:
            "fizz" if 'n' is divisible by 3,
            "buzz" if 'n' is divisible by 5,
            "fizz buzz bazz" if 'n' is divisible by 3 and 5,
            "fizzle" if 'n' is not divisible by either 3 or 5

    EXAMPLE
        helloWhatever(3) => "fizz"
        helloWhatever(5) => "buzz"
        helloWhatever(15) => "fizz buzz bazz"
        helloWhatever(7) => "fizzle"

*/

public class FizzBuzzBazzFizzle {
    public static String fizzBuzzBazzFizzle(int n) {
        return "";
    }
}
