(ns pyramid-assessment-2.core-test
  (:require [clojure.test :refer :all]
            [pyramid-assessment-2.core :refer :all]
            [clojure.set]
            [clojure.test.check :as tc]
            [clojure.test.check.generators :as gen]
            [clojure.test.check.properties :as prop])
  (:use com.rpl.specter)
  (:import (problems SumTheArray StringIntersection CheckEquivalentKeypresses HelloWhatever ReverseTheString MissingNumberInRange FizzBuzzBazzFizzle FlipTheSwitch FindTheMiddle OnlyDigitsInString)))


;; HELPER CODE START
(def current-input (atom 0))
(def tests (atom []))


(defn javaize
  [s]
  (apply str
         (map clojure.string/capitalize
              (clojure.string/split (str s)
                                    #"-"))))

(defn test-passed
  [{pass? :pass?
    points-obtained :points-obtained
    points-possible :points-possible
    your-output :your-output
    expected-output :expected-output
    input :input
    name :test
    tests-passed :tests-passed
    total-tests :total-tests}]

  (if pass?
      {:name name
       :points-obtained points-obtained}
      {:name name
       :points-obtained points-obtained
       :input input
       :points-possible points-possible
       :expected-output expected-output
       :your-output your-output
       :tests-passed tests-passed
       :total-tests total-tests}))

(defn extract-test-data
  []
  (map (fn [test] (test-passed test)) @tests))

(defn output-formatter
  [s]
  (if (= s "")
    (str "\"\"")
    (if (= (type s) String)
      (str "\"" s "\"")
      s)))

(defn pass-fail
  []
  (let [tester-out (fn [{:keys [name points-obtained total-tests points-possible input your-output expected-output tests-passed] :as all}]
                     (if (= (count all) 2)
                       (str "\u2714 " "Passed: " name " Points Gained: " points-obtained "\n")
                       (str (apply str (repeat 10 "╳ ")) "\n"
                            "FAILED: " name
                            "\n    Input: " (output-formatter input)
                            "\n    Your Output: " (output-formatter your-output)
                            "\n    Expected Output: " (output-formatter expected-output)
                            "\n    Points Possible: " points-possible
                            "\n    Points Gained: " points-obtained
                            "\n    Tests Given: " total-tests
                            "\n    Tests Passed: " tests-passed
                            "\n" (apply str (repeat 10 "╳ ")) "\n")))]
    (doseq [test (extract-test-data)]
      (println (tester-out test)))))

(defn total-score
  []
  (let [{po :points-obtained pp :points-possible} (reduce (fn [{po1 :points-obtained pp1 :points-possible}
                                                               {po2 :points-obtained pp2 :points-possible}]
                                                            {:points-possible (+ pp1 pp2)
                                                             :points-obtained (+ po1 po2)})

                                                          @tests)]
    (/ po pp)))

(defn intersection-solution
  [[left right]]
  (if (not (or (= left "") (= right "")))
    (let [pull-nums (fn [s] (into #{} (map #(Integer/parseInt (clojure.string/trim %)) (clojure.string/split s #","))))
          left (pull-nums left)
          right (pull-nums right)
          intersection (sort (clojure.set/intersection left right))
          rest (rest intersection)]
      (apply str (conj (map (partial str ", ") rest) (first intersection))))
    ""))

(defn insert-commas
  [inp]
  (let [[first & rest] inp]
    (apply str (conj (map (fn [i] (str ", " i)) rest) (str first)))))

(defn format-intersection-input
  [inp]
  (transform [ALL] insert-commas (split-at (/ (count inp) 2) inp)))

(defn insert-random-backspaces
  [char-vec]
  (loop [out (map str char-vec)
         possible-backspaces (repeat (rand-int (inc (count out))) "-B")
         insertion-position (rand-int (count out))]
    (if possible-backspaces
      (if (not (> insertion-position (count out)))
        (recur (setval (srange insertion-position insertion-position) ["-B"] out)
               (next possible-backspaces)
               (rand-int (count out)))
        (recur out
               (next possible-backspaces)
               (rand-int (count out))))
      out)))



(defn format-input-check-equivalent-key-presses
  [s-arr]
  (let [left (insert-commas (insert-random-backspaces s-arr))
        right (insert-commas (insert-random-backspaces s-arr))]
    (setval [END] [left] [right])))

(defmacro gen-prop-test
  [name points test-times generator user-function test-function]
  `(swap! tests conj
          (let [{pass#         :pass?
                 tests-passed# :num-tests
                 fail#         :fail} (tc/quick-check ~test-times
                                                      (prop/for-all [i# ~generator]
                                                                    (if-not (nil? i#)
                                                                      (reset! current-input i#))
                                                                    (= ~user-function
                                                                       ~test-function)))]

            (if fail#
               (reset! current-input (first fail#)))
            {:test            ~(javaize name)
             :pass?           pass#
             :total-tests     ~test-times
             :tests-passed    tests-passed#
             :input           @current-input
             :your-output     ~user-function
             :expected-output ~test-function
             :points-possible ~points
             :points-obtained (float (* (/ tests-passed# ~test-times) ~points))})))


(defn check-equivalent-key-presses
  [s]
  (let [back-that-thang-up (fn [x]
                             (reduce (fn [acc next]
                                       (if (or (= next "-B") (= next " -B"))
                                         (apply str (butlast acc))
                                         (str acc next)))
                                     x))
        split (fn [s] (clojure.string/split s  #","))
        [left right] s]
    (= (back-that-thang-up (split left))
       (back-that-thang-up (split right)))))

(defn hello-whatever
  [s]
  (str "Hello, " s))

(defn find-missing-number
  [nums]
  (loop [[a & nth] nums]
    (if (and a nth)
      (if (or (= a (first nth)) (= (inc a) (first nth)))
        (recur nth)
        (inc a))
      0)))

(defn insert-random-missing-integer
  [n]
  (if (<= n 1)
    [0 1]
    (let [range (into [] (range 1 n))
          ith (rand-int (count range))]
      (setval (srange ith (inc ith)) [ith] range))))

(defn fizz-buzz-bazz-fizzle
  [n]
  (cond (and (zero? (mod n 3)) (zero? (mod n 5))) "fizz buzz bazz"
        (zero? (mod n 3)) "fizz"
        (zero? (mod n 5)) "buzz"
        :else "fizzle"))

(defn find-the-middle
  [seq]
  (let [x seq
        [left _] (split-at (/ (count x) 2) x)]
    (if (even? (count x))
      0
      (last left))))

(defn only-digits-in-string?
  [s]
  (if-not (empty? s)
    (= (count (for [c s :when (Character/isDigit c)] c))
       (count s))
    false))

;; HELPER CODE END

;; TESTS

(gen-prop-test check-equivalent-key-presses
               20
               500
               (gen/fmap format-input-check-equivalent-key-presses (gen/such-that not-empty (gen/vector gen/char-alpha)))
               (CheckEquivalentKeypresses/checkEquivalentKeypresses (into-array String @current-input))
               (check-equivalent-key-presses @current-input))

(gen-prop-test find-the-middle
               10
               100
               (gen/vector gen/nat)
               (FindTheMiddle/findTheMiddle (into-array Integer/TYPE @current-input))
               (find-the-middle @current-input))

(gen-prop-test fizz-buzz-bazz-fizzle
               8
               100
               (gen/such-that #(not= % 0) gen/nat)
               (FizzBuzzBazzFizzle/fizzBuzzBazzFizzle @current-input)
               (fizz-buzz-bazz-fizzle @current-input))

(gen-prop-test flip-the-switch
               2
               100
               gen/boolean
               (FlipTheSwitch/flipTheSwitch @current-input)
               (not @current-input))


(gen-prop-test hello-whatever
               5
               100
               gen/string-alpha-numeric
               (HelloWhatever/helloWhatever @current-input)
               (hello-whatever @current-input))

(gen-prop-test missing-number-in-range
               10
               100
               (gen/fmap insert-random-missing-integer (gen/such-that #(not= % 0) gen/nat))
               (MissingNumberInRange/missingNumberInRange (into-array Integer/TYPE @current-input))
               (find-missing-number @current-input))

(gen-prop-test only-digits-in-string
               10
               100
               gen/string-alpha-numeric
               (OnlyDigitsInString/onlyDigitsInString @current-input)
               (only-digits-in-string? @current-input))

(gen-prop-test reverse-the-string
               10
               100
               gen/string-alpha-numeric
               (ReverseTheString/reverseTheString @current-input)
               (apply str (reverse @current-input)))

(gen-prop-test string-intersection
               25
               100
               (gen/fmap format-intersection-input (gen/vector gen/nat))
               (StringIntersection/stringIntersection (into-array String @current-input))
               (intersection-solution @current-input))

(gen-prop-test sum-the-array
               10
               100
               (gen/vector gen/int)
               (SumTheArray/sumTheArray (into-array Integer/TYPE @current-input))
               (apply + @current-input))

;; TEST REPORT

(pass-fail)

(let [s (Math/round (* (total-score) 100))]
  (if (> s 33)
    (println (str "\uD83D\uDC4D" "\uD83D\uDE04"
                  " Score: " s))
    (println (str "\uD83D\uDC4E" "☹️"
                  " Score: " s))))
