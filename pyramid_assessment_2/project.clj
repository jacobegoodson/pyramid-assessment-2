(defproject pyramid_assessment_2 "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [org.clojure/test.check "0.10.0"]
                 [com.rpl/specter "1.1.3"]]
  :java-source-paths ["src"]
  :jvm-opts ["-Dfile.encoding=utf-8"]
  :repl-options {:init-ns pyramid-assessment-2.core})
